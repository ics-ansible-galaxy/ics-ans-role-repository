ics-ans-role-repository
=======================

This Ansible role enables repositories located on Artifactory.

For CentOS:
- CentOS 7 (mirror)
- EPEL (mirror)
- Zabbix (mirror)
- ESS ICS

For Debian:
- Zabbix

Requirements
------------

- ansible >= 2.7
- molecule >= 2.14

Role Variables
--------------

```yaml
repository_centos_version: 7.6.1810
repository_installroot: "/"
repo_zabbix_release: 4.0
```

The `repository_installroot` variable allows to specify an alternative installroot, e.g. /export/nfsroots/centos7/rootfs

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-repository
```

License
-------

BSD 2-clause
