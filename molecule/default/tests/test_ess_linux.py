import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ics-ans-role-repository-ess-linux')


def test_ics_install(host):
    cmd = host.run('dnf install -y gdb')
    assert cmd.rc == 0
