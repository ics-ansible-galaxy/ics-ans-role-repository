import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ics-ans-role-repository-ubuntu-focal')


def test_ethtool_ubuntu_install(host):
    known_package = "ethtool"
    update_result = host.run("apt show " + known_package)
    ics_ubuntu_repo_url = "https://artifactory.esss.lu.se/artifactory/ubuntu-mirror"
    assert ics_ubuntu_repo_url in update_result.stdout
