import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ics-ans-role-repository-ubuntu-jammy')


def test_ethtool_ubuntu_install(host):
    known_package = "ethtool"
    show_result = host.run("apt show " + known_package)
    ubuntu_mirror_url = "https://artifactory.esss.lu.se/artifactory/ubuntu-mirror"
    assert show_result.rc == 0
    assert ubuntu_mirror_url in show_result.stdout


def test_ics_ubuntu_jammy_repository(host):
    known_package = "ptpv2"
    show_result = host.run("apt show " + known_package)
    ics_ubuntu_repo_url = "https://artifactory.esss.lu.se/artifactory/ics-ubuntu"
    assert show_result.rc == 0
    assert ics_ubuntu_repo_url in show_result.stdout
